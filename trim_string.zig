const std = @import("std");

fn trim_string(input: []const u8) []const u8 {
    var begin_pos: usize = 0;
    while(begin_pos < input.len) : (begin_pos += 1) {
        if(!std.ascii.isSpace(input[begin_pos])){
            break;
        }
    }
    var end_pos = input.len;
    while(end_pos > begin_pos) : (end_pos -= 1) {
        if(!std.ascii.isSpace(input[end_pos - 1])){
            break;
        }
    }
    return input[begin_pos..end_pos];
}