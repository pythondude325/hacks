const TOTAL_BITS: u32 = 32;
const EXPONENT_BITS: u32 = 8;
const MANTISSA_BITS: u32 = 23;

const EXPONENT_BIAS: i32 = (1 << (EXPONENT_BITS - 1)) - 1;

pub fn decompose_float(f: f32) -> (bool, i32, u32) {
    let bits = f.to_bits();

    let sign = (bits & (1 << (TOTAL_BITS - 1))) != 0;

    let exponent_bits = (bits >> MANTISSA_BITS) & ((1 << EXPONENT_BITS) - 1);
    let exponent = (exponent_bits as i32) - EXPONENT_BIAS;

    let mut mantissa_bits = bits & ((1 << MANTISSA_BITS) - 1);

    // Add mantissa bit if number isn't subnormal
    if exponent >= core::f32::MIN_EXP - 1 {
        mantissa_bits |= 1 << MANTISSA_BITS;
    }

    (sign, exponent, mantissa_bits)
}

pub fn compose_float(sign: bool, exponent: i32, mantissa: u32) -> f32 {
    let mut result = 0;

    let mantissa_leading_zeros = mantissa.leading_zeros();
    let base_zeros = TOTAL_BITS - MANTISSA_BITS;

    let mut exponent_offset = (base_zeros as i32) - (mantissa_leading_zeros as i32);

    let mut mantissa_bits = mantissa;

    if exponent + exponent_offset < core::f32::MIN_EXP - 1 {
        // Subnormal
        exponent_offset = (core::f32::MIN_EXP - 1) - exponent;
    } else if exponent + exponent_offset > core::f32::MAX_EXP {
        // Infinity
        if sign {
            return core::f32::NEG_INFINITY;
        } else {
            return core::f32::INFINITY;
        }
    }

    // Shift manitssa bits based on exponent offset
    if exponent_offset > 1 {
        mantissa_bits >>= exponent_offset;
    } else if exponent_offset < 1 {
        mantissa_bits <<= -exponent_offset;
    }

    // clear out the implicit bit
    mantissa_bits &= (1 << MANTISSA_BITS) - 1;

    result |= mantissa_bits;

    let biased_exponent = exponent + exponent_offset - 1 + EXPONENT_BIAS;
    // Check if exponent is within range; Handle subnormal numbers
    result |= (biased_exponent as u32) << MANTISSA_BITS;

    result |= (sign as u32) << (TOTAL_BITS - 1);

    f32::from_bits(result)
}

fn next_float(f: f32) -> Option<f32> {
    if f == core::f32::INFINITY {
        println!("infinity");
        return None;
    }

    let (is_negative, exponent, mut mantissa) = decompose_float(f);

    println!("s: {:?}, e: {}, m: {:032b}", is_negative, exponent, mantissa);

    if is_negative {
        mantissa -= 1;
    } else {
        mantissa += 1;
    }

    Some(compose_float(is_negative, exponent, mantissa))
    }

fn main() {
    let f: f32 = 0.000000000000000000000000000000000000011754942;
    let r = next_float(f);
    println!("f: {}\nr: {:?}", f, r);

    println!("{:032b}", core::f32::NEG_INFINITY.to_bits());

}
