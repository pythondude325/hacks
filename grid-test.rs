use rand::prelude::*; // 0.7.2
use itertools::Itertools; // 0.8.0
use std::iter;

mod grid {
    use std::fmt;
    use std::ops::{Index, IndexMut};

    pub struct Grid<T> {
        inner: [[T; 16]; 16],
    }

    impl<T: Copy> Grid<T> {
        pub fn new(t: T) -> Grid<T> {
            Grid {
                inner: [[t; 16]; 16],
            }
        }
    }

    impl<T> Index<(usize, usize)> for Grid<T> {
        type Output = T;

        fn index(&self, index: (usize, usize)) -> &T {
            &self.inner[index.0][index.1]
        }
    }

    impl<T> IndexMut<(usize, usize)> for Grid<T> {
        fn index_mut(&mut self, index: (usize, usize)) -> &mut T {
            &mut self.inner[index.0][index.1]
        }
    }

    impl fmt::Display for Grid<char> {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            for row in &self.inner {
                for cell in row {
                    write!(f, "{}", cell)?;
                }
                write!(f, "\n")?;
            }
            Ok(())
        }
    }
}
use grid::Grid;

/*
struct Zipper<A: Iterator, B: Iterator>(A, B);
impl<A, B, T, U> Iterator for Zipper<A, B> where A: Iterator<Item = T>, B: Iterator<Item = U> {
    type Item = (T, U);

    fn next(&mut self) -> Option<(T, U)> {
        match (self.0.next(), self.1.next()) {
            (Some(t), Some(u)) => Some((t, u)),
            _ => None
        }
    }
}
*/

fn main() {
    let mut g = Grid::new(' ');

    let grid_write = |(coord, character)| g[coord] = character;

    (0..16).cartesian_product(0..16).zip(iter::repeat_with(|| ['/', '\\'][random::<usize>() % 2])).for_each(grid_write);


    let points = (1..=16).map(|_| (random::<usize>() % 16, random::<usize>() % 16)).collect::<Vec<_>>();

    points.iter()
        .cartesian_product(points.iter())
        .filter(|(a, b)| a != b)
        .map(|(a, b)| ((a.0+b.0)/2, (a.1+b.1)/2))
        .zip(iter::repeat('-'))
        .for_each(grid_write);

    points.iter().copied().zip(iter::repeat('#')).for_each(grid_write);

    println!("{}", g);
}

