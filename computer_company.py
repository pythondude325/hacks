import random

prefixes = [
    "info", "macro", "data", "tele", "super", "sys", "inter", "intra", "meta", "uni", "auto", "para", "ultra", 
    "epi", "compu", "graphi", "stat", "wave", "metro", "crypto", "trans", "net", "nova",
    "cyber", "digi",
    "lab", "gene", "bio",
    "zetta", "exa", "peta", "tera", "giga", "mega", "kilo",
    "milli", "micro", "nano", "pico", "femto", "atto", "zepto"
]

postfixes = [
    "soft", "com", "co", "tel", "tec", "tech", "dyne", "disk", "serve",
    "ware", "pro", "systems", "net", "max", "point", "storm", "light", "star", "byte",
    "matics",
]

fields = [
    "software", "research", "solutions", "technology", "systems", "group", "industries", "international"
]

def generate_company_name():
    company_name = random.choice(prefixes) + random.choice(postfixes)
    if random.random() < 0.25:
        company_name += " " + random.choice(fields)
    return company_name

if __name__ == "__main__":
    for _ in range(20):
        print(generate_company_name())
