#lang racket/base

(require racket/match racket/string xml)

(define/match (xmlify-arg a)
  [((? number? n)) (number->string (exact->inexact n))]
  [((? symbol? s)) (symbol->string s)]
  [((? string? s)) s]
  [((list a ...)) (string-join (map xmlify-arg a) " ")])

#;(define-syntax transformation
  (syntax-rules ()
    ((_ (name values ...) ...)
     `((,(symbol->string 'name) "(" ,values ... ")") ...))))

(define-syntax transformation
  (syntax-rules ()
    ((_ (name values ...) ...)
     `(,(format "~a(~a)" 'name (xmlify-arg (list values ...))) ...))))

(define/match (xmlify x)
  [((list tag args content ...))
   (list* tag
         (map (lambda (arg) (list (car arg) (xmlify-arg (cadr arg)))) args)
         (map xmlify content))]
  [((? string? s)) s])


(define black-color "#222")
(define red-color "firebrick")
(define gear-color "gold")
(define gear-radius 6)
(define gear-width (* gear-radius 1/5))
(define gear-tooth-width (* gear-radius 1/5 5/4))
(define gear-tooth-count 12)

(define doc
  `(svg ((viewBox (0 0 20 20)) (xmlns "http://www.w3.org/2000/svg"))
        (rect ((height 20)
               (width 20)
               (fill ,black-color)))
        
        (path ((d ((M 20 20)
                   (H 0)
                   (V 0)
                   (z)))
               (fill ,red-color)))

        (circle ((r ,gear-radius)
                 (cx 10)
                 (cy 10)
                 (fill "none")
                 (stroke ,gear-color)
                 (stroke-width ,gear-width)))

        ,@(for/list ([i gear-tooth-count])
            `(rect ((height ,gear-tooth-width)
                    (width ,gear-tooth-width)
                    (transform ,(transformation (translate 10 10)
                                                (rotate (* 360 (/ gear-tooth-count) i))
                                                (translate (/ gear-tooth-width -2)
                                                           gear-radius)))
                    (fill ,gear-color))))

        ))

(current-output-port (open-output-file "logo.svg" #:mode 'text #:exists 'truncate))

(write-xml/content (xexpr->xml (xmlify doc)))

(close-output-port (current-output-port))
