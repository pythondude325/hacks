// BF Interpreter

use std::convert::TryFrom;
use std::collections::BTreeMap;
use std::io::{self, Read};

type Cell = i64;

#[derive(Debug)]
struct Executor {
    tape: BTreeMap<isize, Cell>,
    tape_position: isize,
    instructions: Vec<char>,
    ip: usize,
    bracket_map: BTreeMap<usize, usize>
}

impl Executor {
    pub fn new(prog: &str) -> Executor {
        let instructions = prog.chars().filter(|c| match c {
            '>' | '<' | '+' | '-' | '.' | ',' | '[' | ']' => true,
            _ => false
        }).collect::<Vec<_>>();

        let mut bracket_map = BTreeMap::<usize, usize>::new();
        let mut bracket_stack = Vec::new();
        for (idx, &inst) in instructions.iter().enumerate() {
            match inst {
                '[' => {
                    bracket_stack.push(idx);
                }
                ']' => {
                    let matched_bracket_idx = bracket_stack.pop().unwrap();

                    bracket_map.insert(matched_bracket_idx, idx);
                    bracket_map.insert(idx, matched_bracket_idx);
                }
                _ => {}
            }
        }

        Executor {
            tape: BTreeMap::new(),
            tape_position: 0,
            instructions,
            ip: 0,
            bracket_map
        }
    }

    fn get_tape_value(&self) -> Cell {
        self.tape.get(&self.tape_position).copied().unwrap_or_default()
    }

    fn step(&mut self) {
        let instruction = self.instructions[self.ip];

        match instruction {
            '>' => {
                self.tape_position += 1;
            }
            '<' => {
                self.tape_position -= 1;
            }
            '+' => {
                let value = self.get_tape_value();
                self.tape.insert(self.tape_position, value + 1);
            }
            '-' => {
                let value = self.get_tape_value();
                self.tape.insert(self.tape_position, value - 1);
            }
            '.' => {
                print!("{}", char::try_from(u32::try_from(self.get_tape_value()).unwrap()).unwrap());
            }
            ',' => {
                let mut b: [u8; 1] = [0];

                let new_value = if io::stdin().read_exact(&mut b).is_ok() {
                    i64::from(b[0])
                } else {
                    -1
                };

                self.tape.insert(self.tape_position, new_value);
            }
            '[' => {
                if self.get_tape_value() == 0 {
                    self.ip = self.bracket_map[&self.ip];
                }
            }
            ']' => {
                if self.get_tape_value() != 0 {
                    self.ip = self.bracket_map[&self.ip];
                }
            }
            _ => {}
        }

        self.ip += 1;
    }

    pub fn run(mut self) {
        let len = self.instructions.len();
        while self.ip < len {
            self.step();
        }
    }
}

const HELLO_WORLD: &str = "++++++++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.>---.+++++++..+++.>>.<-.<.+++.------.--------.>>+.>++.";

const ROT13: &str = r#"
-,+[                         Read first character and start outer character reading loop
    -[                       Skip forward if character is 0
        >>++++[>++++++++<-]  Set up divisor (32) for division loop
                               (MEMORY LAYOUT: dividend copy remainder divisor quotient zero zero)
        <+<-[                Set up dividend (x minus 1) and enter division loop
            >+>+>-[>>>]      Increase copy and remainder / reduce divisor / Normal case: skip forward
            <[[>+<-]>>+>]    Special case: move remainder back to divisor and increase quotient
            <<<<<-           Decrement dividend
        ]                    End division loop
    ]>>>[-]+                 End skip loop; zero former divisor and reuse space for a flag
    >--[-[<->+++[-]]]<[         Zero that flag unless quotient was 2 or 3; zero quotient; check flag
        ++++++++++++<[       If flag then set up divisor (13) for second division loop
                               (MEMORY LAYOUT: zero copy dividend divisor remainder quotient zero zero)
            >-[>+>>]         Reduce divisor; Normal case: increase remainder
            >[+[<+>-]>+>>]   Special case: increase remainder / move it back to divisor / increase quotient
            <<<<<-           Decrease dividend
        ]                    End division loop
        >>[<+>-]             Add remainder back to divisor to get a useful 13
        >[                   Skip forward if quotient was 0
            -[               Decrement quotient and skip forward if quotient was 1
                -<<[-]>>     Zero quotient and divisor if quotient was 2
            ]<<[<<->>-]>>    Zero divisor and subtract 13 from copy if quotient was 1
        ]<<[<<+>>-]          Zero divisor and add 13 to copy if quotient was 0
    ]                        End outer skip loop (jump to here if ((character minus 1)/32) was not 2 or 3)
    <[-]                     Clear remainder from first division if second division was skipped
    <.[-]                    Output ROT13ed character from copy and clear it
    <-,+                     Read next character
]                            End character reading loop
"#;

fn main() {
    Executor::new(ROT13).run();
}
