#!/usr/bin/env python

# Copyright (c) 2018 Graham Scheaffer
# MIT License

import sys
import argparse
import json
import base64
import tarfile

parser = argparse.ArgumentParser()
parser.add_argument("project", help="The .vex file you want to extract from")
action_group = parser.add_mutually_exclusive_group(required=True)
action_group.add_argument("-l", "--list", help="list files inside the .vex project", action="store_true")
action_group.add_argument("-p", "--print", metavar="file", help="print the selected internal files", nargs="+", dest="print_files", default=None, type=str)

args = parser.parse_args()

with tarfile.open(args.project, "r") as vex_file:
    members = vex_file.getmembers()
    with vex_file.extractfile(members[0]) as data_file:
        data = json.load(data_file)

        internal_files = data['files']

        if args.list:
            for file_name in internal_files:
                print(file_name)

        if args.print_files != None:
            for file_name in args.print_files:
                file_data = base64.b64decode(internal_files[file_name]).decode("utf-8")
                print(file_data)




