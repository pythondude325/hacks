use std::ops::Add;

type Int = u128;

struct FibonacciIterator<T>(T, T);

impl FibonacciIterator<Int> {
    fn new() -> FibonacciIterator<Int> {
        FibonacciIterator(1, 1)
    }
}

impl<T: Add<T, Output = T> + Eq + Copy> Iterator for FibonacciIterator<T> {
    type Item = T;

    fn next(&mut self) -> Option<T> {
        let number = self.0 + self.1;

        self.0 = self.1;
        self.1 = number;

        Some(number)
    }
}

fn main() {
    for (index, num) in FibonacciIterator::<Int>::new().enumerate() {
        println!("{}: {}", index, num);
    }
}
