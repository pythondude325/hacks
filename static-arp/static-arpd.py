#!/usr/bin/python

import os
import sys
import re
import pathlib
import yaml

usage = "Usage: {} COMMAND\nCommand may be start, stop, or restart.".format(sys.argv[0])

def error(message):
  print(message)
  exit(1)

if os.geteuid() != 0:
  error("ERROR: Must be run as root.")

if len(sys.argv) != 2:
  error(usage)

command = sys.argv[1]
if command not in ["start", "stop", "restart"]:
  error(usage)

start = command in ["start", "restart"]
stop = command in ["stop", "restart"]

config = {}

possible_paths = ["/etc/static-arp.yml", "./static-arp.yml"]

for path_string in possible_paths:
  path = pathlib.Path(path_string)
  if path.is_file():
    config = yaml.load(path.open())

if config == {}:
  error("ERROR: No configuration file")

for entry in config['entries']:
  ip = entry['ip']
  mac = entry['mac']
  interface = entry['interface']

  if not re.match(r"^([0-9a-fA-F][0-9a-fA-F]:){5}([0-9a-fA-F][0-9a-fA-F])$", mac):
    error("ERROR: {} is not a valid mac address.".format(mac))

  # TODO: Check if ip address is valid ipv4 or ipv6

  if stop:
    print("ip neigh del {} dev {}".format(ip, interface))
  
  if start:
    print("ip neigh replace {} lladdr {} dev {}".format(ip, mac, interface))