#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <dlfcn.h>
#include <sys/mman.h>

// https://x-c3ll.github.io/posts/fileless-memfd_create/

void *exec(char *symbol, char *program){
  int return_value;
	
  int so_file = memfd_create("shared_object", 0);

  if(so_file < 0) return NULL;

  char so_file_path[64];
  sprintf(so_file_path, "/proc/%d/fd/%d", getpid(), so_file);

  char command[512];
  sprintf(command, "gcc -xc - -shared -fPIC -o %s", so_file_path);
  
  FILE* process = popen(command, "w");
  fputs(program, process); // Write program to gcc's stdin
  return_value = pclose(process);
  if(return_value != 0) return NULL;
  
  void *library = dlopen(so_file_path, RTLD_NOW);
  
  close(so_file);

  return dlsym(library, symbol);
}

int main() {
  void (*say_hello)(void) = exec("say_hello", "   \n\
    #include <stdio.h>                              \n\
    void say_hello(void){                           \n\
      printf(\"Hello World\\n\");                   \n\
    }                                               \n\
  ");

  if(say_hello == NULL) return 1;

  say_hello();


  return 0;
}
