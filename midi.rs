use std::ops;

#[derive(Clone, Copy, Debug)]
struct MidiNote(pub f64);

impl MidiNote {
    const REFERENCE_NOTE: f64 = 69.0;
    const REFERENCE_FREQUENCY: f64 = 440.0;

    fn new(n: impl Into<f64>) -> MidiNote {
        MidiNote(n.into())
    }

    fn from_frequency(freq: f64) -> MidiNote {
        MidiNote::new(12.0 * (freq / Self::REFERENCE_FREQUENCY).log2() + Self::REFERENCE_NOTE)
    }

    fn to_frequency(&self) -> f64 {
        Self::REFERENCE_FREQUENCY * ((self.0 - Self::REFERENCE_NOTE) / 12.0).exp2()
    }
}

impl ops::Add for MidiNote {
    type Output = MidiNote;
    fn add(self, rhs: MidiNote) -> MidiNote {
        MidiNote::new(self.0 + rhs.0)
    }
}

fn ratio_grade(r: f64) -> f64 {
    (r - r.round()).abs()
}

fn test_scale_ratios() {
    let root = MidiNote::new(60);
    for value in (0..=12).map(MidiNote::new) {
        let note = root + value;

        let note_ratio = note.to_frequency() / root.to_frequency();

        let best_ratio: (f64, f64) = (1..11)
            .map(|denominator| (note_ratio * (denominator as f64), denominator as f64))
            .min_by(|(rn1, _), (rn2, _)| ratio_grade(*rn1).partial_cmp(&ratio_grade(*rn2)).unwrap())
            .unwrap();

        let ratio_error = best_ratio.0 - best_ratio.0.round();

        println!(
            "Best ratio for {} is {}/{} (error: {})",
            value.0,
            best_ratio.0.round(),
            best_ratio.1,
            ratio_error
        );
    }
}

fn main() {
    println!("{}", MidiNote::new(60).to_frequency());

    test_scale_ratios();
}

