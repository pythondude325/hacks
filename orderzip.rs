use std::cmp::Ordering;
use std::iter;

// The iterators must be ordered
//
//
// Self: Iterator<Item = T>
// T: Ord
//
// a = self.next()
// b = self.next()
// a < b

struct OrderZip<T, I, J>(iter::Peekable<I>, iter::Peekable<J>)
where
    T: Ord,
    I: Iterator<Item = T>,
    J: Iterator<Item = T>;

impl<T: Ord, I: Iterator<Item=T>, J: Iterator<Item=T>> OrderZip<T, I, J> {
    fn new(a: I, b: J) -> OrderZip<T, I, J> {
        OrderZip(a.peekable(), b.peekable())
    }
}

impl<T: Ord, I: Iterator<Item = T>, J: Iterator<Item=T>> Iterator for OrderZip<T, I, J> {
    type Item = (Option<T>, Option<T>);

    fn next(&mut self) -> Option<(Option<T>, Option<T>)> {
        match (self.0.peek(), self.1.peek()) {
            (None, None) => None,
            (Some(_), None) => Some((self.0.next(), None)),
            (None, Some(_)) => Some((None, self.1.next())),
            (Some(a), Some(b)) => match a.cmp(b) {
                Ordering::Less => Some((self.0.next(), None)),
                Ordering::Equal => Some((self.0.next(), self.1.next())),
                Ordering::Greater => Some((None, self.1.next())),
            },
        }
    }
}

struct Union<T: Ord, I: Iterator<Item=T>, J: Iterator<Item=T>>(OrderZip<T, I, J>);

impl<T: Ord, I: Iterator<Item=T>, J: Iterator<Item=T>> Iterator for Union<T, I, J> {
    type Item = T;

    fn next(&mut self) -> Option<T> {
        match self.0.next()? {
            (None, Some(x)) | (Some(x), Some(_)) | (Some(x), None) => Some(x),
            _ => None
        }
    }
}

struct Intersection<T: Ord, I: Iterator<Item=T>, J: Iterator<Item=T>>(OrderZip<T, I, J>);

impl<T: Ord, I: Iterator<Item=T>, J: Iterator<Item=T>> Iterator for Intersection<T, I, J> {
    type Item = T;

    fn next(&mut self) -> Option<T> {
        match self.0.next()? {
            (Some(x), Some(_)) => Some(x),
            _ => self.next(),
        }
    }
}

struct Difference<T: Ord, I: Iterator<Item=T>, J: Iterator<Item=T>>(OrderZip<T, I, J>);

impl<T: Ord, I: Iterator<Item=T>, J: Iterator<Item=T>> Iterator for Difference<T, I, J> {
    type Item = T;

    fn next(&mut self) -> Option<T> {
        match self.0.next()? {
            (Some(x), None) => Some(x),
            _ => self.next(),
        }
    }
}

struct SymetricDifference<T: Ord, I: Iterator<Item=T>, J: Iterator<Item=T>>(OrderZip<T, I, J>);

impl<T: Ord, I: Iterator<Item=T>, J: Iterator<Item=T>> Iterator for SymetricDifference<T, I, J> {
    type Item = T;

    fn next(&mut self) -> Option<T> {
        match self.0.next()? {
            (Some(x), None) | (None, Some(x)) => Some(x),
            _ => self.next(),
        }
    }
}

fn main() {
    let a = vec![1, 3, 4, 5];
    let b = vec![2, 3, 5, 6];

    for item in SymetricDifference(OrderZip::new(a.iter(), b.iter())) {
        println!("{:?}", item);
    }

}

