use std::thread;
use std::time::Duration;
use std::collections::BTreeSet;
use std::sync::{Arc, RwLock};
use std::sync::atomic::{AtomicU64, Ordering};

type Int = u64;

const THREAD_COUNT: u64 = 4;

fn main() {
    let prime_numbers = Arc::new(RwLock::new(BTreeSet::<Int>::new()));
    let next_check = Arc::new(AtomicU64::new(2));

    let state = (prime_numbers.clone(), next_check.clone());
    thread::spawn(move || {
        let (prime_numbers, _next_check) = state;

        loop {
            thread::sleep(Duration::from_millis(5_000));

            println!("{:?}", *prime_numbers.read().unwrap());
        }
    });

    for thread_index in 0..THREAD_COUNT {
        let state = (prime_numbers.clone(), next_check.clone());
        thread::spawn(move || {
            let (prime_numbers, next_check) = state;

            loop {
                let current_check = next_check.fetch_add(1, Ordering::Relaxed);
                let current_check_root = (current_check as f64).sqrt() as u64;

                if prime_numbers.read().unwrap().range(0..current_check_root).all(|i| current_check % i != 0) {
                    prime_numbers.write().unwrap().insert(current_check);
                }
            }
        });

        loop {
            let temp = prime_numbers.read().unwrap();
            let last = temp.iter().next_back();

            let next = next_check.load(Ordering::Relaxed);

            if last.is_some() {
                let last_prime = last.unwrap();
                if last_prime * last_prime <= next {
                    break;
                }
            }
            thread::sleep(Duration::from_millis(100));
        }
    }
}
