trait ToUsize {
    fn to_usize() -> usize;
}

// impl ToUsize for ! {
//     fn to_usize() -> usize {
//         0
//     }
// }

impl ToUsize for () {
    fn to_usize() -> usize {
        1
    }
}

impl ToUsize for bool {
    fn to_usize() -> usize {
        2
    }
}

impl<A: ToUsize, B: ToUsize> ToUsize for Result<A, B> {
    fn to_usize() -> usize {
        A::to_usize() + B::to_usize()
    }
}

impl<A: ToUsize, B: ToUsize> ToUsize for (A, B) {
    fn to_usize() -> usize {
        A::to_usize() * B::to_usize()
    }
}

impl<A: ToUsize, B: ToUsize> ToUsize for fn(A) -> B {
    fn to_usize() -> usize {
        B::to_usize().pow(A::to_usize() as u32)
    }
}

impl<A: ToUsize> ToUsize for Option<A> {
    fn to_usize() -> usize {
        A::to_usize() + 1
    }
}

fn main() {

}
